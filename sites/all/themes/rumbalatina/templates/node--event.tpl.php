<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
 ?>
<?php if ($teaser): ?>
<div id="node-<?php print $node->nid; ?>" class="panel panel-teaser">
	<div class="panel-heading">
		<?php print render($title_prefix); ?>
		<h2><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
		<?php print render($title_suffix); ?>
	</div>
	<div class="panel-body">
		<?php print render($content['field_teaser_image']); ?>
	</div>
	<ul class="list-group">
		<li class="list-group-item list-group-item-date"><?php print render($content['field_date']); ?></li>
		<li class="list-group-item list-group-item-venue"><?php print render($content['field_venue']); ?></li>
		<li class="list-group-item list-group-item-tags"><?php print render($content['field_tags']); ?></li>
	</ul>
</div>
<?php else: ?>
	<?php
		function time_elapsed_string($datetime, $full = false) {
		$now = new DateTime;
		$ago = new DateTime($datetime);
		$diff = $now->diff($ago);

		$diff->w = floor($diff->d / 7);
		$diff->d -= $diff->w * 7;

		$string = array(
		'y' => 'year',
		'm' => 'month',
		'w' => 'week',
		'd' => 'day',
		'h' => 'hour',
		'i' => 'minute',
		's' => 'second',
		);
		foreach ($string as $k => &$v) {
		if ($diff->$k) {
		$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
		} else {
		unset($string[$k]);
		}
		}

		if (!$full) $string = array_slice($string, 0, 1);
			return $string ? implode(', ', $string) . ' ago' : 'just now';
		}
		$eventid = $node->field_fbeid['und'][0]['safe_value'];
		$albumid = $node->field_album['und'][0]['safe_value'];
		if($eventid != ""){
			function facebook($scope, $fields){
				$access_token = "1632400760310371|vU5bknr4iM0x6kIrGa6rQaDxU5c";
				$json_link = "https://graph.facebook.com/{$scope}/?fields={$fields}&access_token={$access_token}";
				$json = file_get_contents($json_link);
				$facebook = json_decode($json, true, 512, JSON_BIGINT_AS_STRING);
				return $facebook;
			}	
			$event_count = facebook($eventid, "attending.summary(true).limit(0)")['attending']['summary']['count'];
			$event_feed = facebook($eventid . "/feed", "updated_time,message,from&limit=50")['data'];
		}
		
		$todays_date = date("Ymd");
		$event_date = strtotime($node->field_date[LANGUAGE_NONE][0]['value']);
		$date_set = true;
		if($todays_date > date("Ymd", strtotime($node->field_date[LANGUAGE_NONE][0]['value']))){
			$date_set = false;
		}
	?>
	<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
		<div class="row">
			<div class="col-md-8">
				<?php print render($title_prefix); ?>
					<h1><?php print $title; ?></h1>
				<?php print render($title_suffix); ?>
			</div>
			<div class="col-md-4">
				<?php if($date_set):?>
					<button type="button" class="btn btn-yellow">
						<?php print date("d/m/Y g:iA", $event_date); ?>
					</button>
				<?php endif; ?>
			</div>
			<div class="col-xs-12">
				<?php print render($content['field_image']); ?>
			</div>
			<div id="content" class="col-sm-8">
				<?php print render($content['body']); ?>
				<div class="row">
					<div class="col-sm-4">
						<h2>On the night</h2>
						<?php print render($content['field_on_the_night']); ?>
					</div>
					<div class="col-sm-4">
						<h2>Details</h2>
						<?php print render($content['field_details']); ?>
					</div>
					<div class="col-sm-4">
						<h2>Prices &amp; VIPs</h2>
						<?php print render($content['field_prices']); ?>
					</div>
				</div>
				<?php if ($eventid != "" && $date_set): ?>
					<div class="row">
						<div class="col-md-8">
								<h2>Messages</h2>
								<?php foreach ($event_feed as &$msg): ?>
									<?php if (($msg['from']['id'] == 170168399703739) && ($msg['message'] != "")):?>
										<hr/>
										<p>
											<?php print $msg['message']; ?><br/>
											<small class="yellow"><?php print date('d/m/Y', strtotime($msg['updated_time'])); ?> - via <a href="http://www.facebook.com/<?php print $eventid; ?>" target="_blank">Facebook</a></small>
										</p>
									<?php endif; ?>
								<?php endforeach; ?>
								<hr/>
						</div>
					</div>
				<?php endif; ?>
				<?php if($albumid != ''): ?>
					<div id="event-photos" class="row">
						<div class="col-xs-12">
							<h2>Photos from the event</h2>
						</div>
						<?php print render($content['field_photos']); ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-sm-4">
				<div class="well">
					<p class="large"><?php print $event_count; ?> attend<?php print $date_set ? 'ing' : 'ed';?></p>
				</div>
				<?php print render($content['field_venue']); ?>
				<?php if ($date_set):?>
					<button type="button" class="btn btn-yellow">Buy Tickets</button>
				<?php endif; ?>
				<a href="http://www.facebook.com/<?php print $eventid; ?>" class="btn btn-blue" target="_blank">Go to the Facebook event</a>
				<?php print render($content['field_poster']); ?>
				<?php print render($content['field_tags']); ?>
			</div>
		</div>
	</div>
<?php endif; ?>
