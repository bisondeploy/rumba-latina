<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<div id="carousel" class="carousel slide" data-ride="carousel">
	<ol class="carousel-indicators">
		<?php foreach ($rows as $i => $ind): ?>
			<li data-target="#carousel" data-slide-to="<?php print $i; ?>"></li>
		<?php endforeach; ?>
	</ol>
	<div class="carousel-inner" role="listbox">
	<?php foreach ($rows as $id => $row): ?>
		<div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .' '. ($id == 0 ? 'active' : null).'"';  } ?>>
			<?php print $row; ?>
		</div>
	<?php endforeach; ?>
	</div>
	<a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="right carousel-control" href="#carousel" role="button" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>
</div>