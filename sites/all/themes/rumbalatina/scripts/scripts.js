﻿(function ($) {
	$(document).ready(function () {

		$('#social-menu li a').each(function () {
			var str = $(this).text();
			var first = str.substring(0, 11);
			var last = str.substring(11, 100);
			var string = "<span class='yellow'>" + first + "</span>" + last;
			$(this).html(string);
		});

		$('.grid').masonry({
			itemSelector: '.grid-item'
		});
		console.log('test');
		$('#event-photos a').colorbox({ rel: "group3", transition: "elastic", height: "50%" });

	});
})(jQuery)