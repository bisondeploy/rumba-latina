<?php

// -------------------------------------------------------
// -------- Remove annoying css set by modules
// -------------------------------------------------------

function rumbalatina_css_alter(&$css) {
	$items = array(
		'modules/system/system.menus.css' => FALSE,
		'modules/system/system.messages.css' => FALSE,
		'modules/system/system.theme.css' => FALSE,
		'modules/comment/comment.css' => FALSE,
		'modules/field/theme/field.css' => FALSE,
		'modules/node/node.css' => FALSE,
		'modules/search/search.css' => FALSE,
		'modules/user/user.css' => FALSE,
	);
	if(user_is_logged_in() != 1){
		array_push($items, array('modules/system/system.base.css' => FALSE));
	}
	$css = array_diff_key($css, $items);
}

// -------------------------------------------------------
// -------- Modify JS output
// -------------------------------------------------------

function rumbalatina_js_alter(&$javascript) {
	$javascript ['misc/jquery.js']['data'] = drupal_get_path('theme', 'rumbalatina') . '/scripts/jquery.js';
}

// -------------------------------------------------------
// -------- Remove height and width attributes from IMG
// -------------------------------------------------------

function rumbalatina_preprocess_image(&$variables) {
  foreach (array('width', 'height') as $key) {
    unset($variables[$key]);
  }
  // as all images are stored in DFB, this removes the
  $variables['path'] = str_replace("dragonfruitbeats.com.au/", "", $variables['path']);
}

// -------------------------------------------------------
// -------- Domain
// -------------------------------------------------------
