<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page while offline.
 *
 * All the available variables are mirrored in html.tpl.php and page.tpl.php.
 * Some may be blank but they are provided for consistency.
 *
 * @see template_preprocess()
 * @see template_preprocess_maintenance_page()
 *
 * @ingroup themeable
 */
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
	<head profile="<?php print $grddl_profile; ?>">
		<!--?php print $head; ?-->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php print $head_title; ?></title>
		<?php print $styles; ?>
		<?php print $scripts; ?>
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="maintenance" <?php print $attributes;?>>
		<div class="container container-thin">
			<div class="row">
				<div class="col-xs-12">
					<?php if (!empty($logo)): ?>
						<a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>" rel="home">
							<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" class="center-block" />
						</a>
					<?php endif; ?>
				</div>
				<div class="col-xs-12">
					<h1><p>Coming</p> <p>soon</p></h1>
					<p><?php print render($content); ?></p>
					<?php $menu = menu_navigation_links('menu-social'); print theme('links__menu_social', array('links' => $menu, 'attributes' => array('id' => 'social-menu', 'class' => array('nav', 'nav-justified', 'nav-social'), 'target' => '_blank'))); ?>
				</div>
			</div>
		</div>
	</body>
</html>