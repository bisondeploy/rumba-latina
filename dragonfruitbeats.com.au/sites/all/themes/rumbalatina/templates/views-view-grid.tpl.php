<?php

/**
 * @file
 * Default simple view template to display a rows in a grid.
 *
 * - $rows contains a nested array of rows. Each row contains an array of
 *   columns.
 *
 * @ingroup views_templates
 */
?>
<?php foreach ($rows as $row_number => $columns): ?>
	<div class="row grid">
		<?php foreach ($columns as $column_number => $item): ?>
		<div class="col-sm-4 col-md-3 grid-item">
			<?php print $item; ?>
		</div>
		<?php endforeach; ?>
	</div>
<?php endforeach; ?>